SHELL = /bin/bash

build_tag ?= apache2

.PHONY : build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) .

.PHONY : clean
clean:
	echo 'no-op'

.PHONY : test
test:
	build_tag=$(build_tag) ./test/run
