FROM debian:bookworm

SHELL ["/bin/bash", "-c"]

ENV ACME_SERVER=https://locksmith.oit.duke.edu/acme/v2/directory \
    APACHE_LOG_LEVEL=warn \
    APACHE_MD_STORE_DIR=/opt/apache2/md \
    APACHE_OPT_CONF_DIR=/opt/apache2/conf \
    APACHE_SERVER_ADMIN=library-system-administration@duke.edu \
    APACHE_SERVER_NAME=localhost \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    TZ=US/Eastern

# Debain defaults set in /etc/apache2/envvars
# APACHE_RUN_USER=www-data
# APACHE_RUN_GROUP=www-data

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install \
	  apache2 \
	  apache2-utils \
	  less \
          libapache2-mod-auth-openidc \
          libapache2-mod-oauth2 \
	  libapache2-mod-shib \
	  libapache2-mod-xsendfile \
	  locales \
	  lynx \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/* ; \
	echo "${LANG} UTF-8" >> /etc/locale.gen ; \
	locale-gen $LANG

WORKDIR /etc/apache2

COPY ./apache2/ ./
COPY ./bin/ /usr/local/bin/

RUN set -eux; \
	a2enmod \
	  headers \
	  info \
	  md \
	  proxy \
	  proxy_connect \
	  proxy_http \
	  proxy_wstunnel \
	  rewrite \
	  ssl \
	; \
	a2disconf \
	  other-vhosts-access-log \
	  serve-cgi-bin \
 	  security \
	; \
	a2enconf global; \
	a2dissite 000-default; \
	a2ensite 80-vhost; \
	chmod +x /usr/local/bin/start-fg; \
	mkdir -p /opt/apache2/conf /opt/apache2/md

# https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
STOPSIGNAL SIGWINCH

VOLUME /var/run/shibboleth \
	/var/www/html \
	/opt/apache2/conf \
	/opt/apache2/md

HEALTHCHECK CMD apachectl fullstatus

EXPOSE 80 443

CMD [ "/usr/local/bin/start-fg" ]
